class Reservation < ActiveRecord::Base

  validates :table_id, numericality: true, :presence => true
  validates :start_time, :end_time, :presence => true

  validates_each :start_time, :end_time do |record, attr, value|
    unless record.send('is_time?', value)
      record.errors.add(attr, 'must be a valid datetime')
    end
  end

  validate :time_correct_values?, :on => [:create, :update], :if => 'input_time_valid?'

  validate :overlap_with_any?, :on => [:create, :update], :if => 'input_time_valid?'

  private
  
  def is_time? val
    val.present? && ((DateTime.parse(val.to_s) rescue ArgumentError) != ArgumentError)
  end

  def input_time_valid?
    is_time?(start_time) && is_time?(end_time)
  end

  def time_correct_values?
    errors.add(:id, 'end time has to be bigger than start time') if start_time > end_time
    errors.add(:id, 'end time can\'t be equal start time') if start_time == end_time
  end

  def overlap_with_any?
    if Reservation.where(table_id: table_id).exists?(['NOT(start_time >= ? OR end_time <= ?)', end_time, start_time])
      errors.add(:id, 'this time of this table in reserve')
    end
  end

end