FactoryGirl.define do

  factory :reservation, class: Reservation do
    table_id 7
    start_time DateTime.now
    end_time DateTime.now + 2.hours
  end

end
