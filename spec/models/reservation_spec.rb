require 'spec_helper'

describe Reservation do

  before(:each) do
    create(:reservation)
    @new_reservation = build(:reservation)
  end

  it { should validate_presence_of(:table_id).with_message('can\'t be blank')}
  it { should validate_presence_of(:start_time).with_message('can\'t be blank')}
  it { should validate_presence_of(:end_time).with_message('can\'t be blank')}

  it { should allow_value(DateTime.now).for(:start_time) }
  it { should allow_value(DateTime.now).for(:end_time) }

  it { should_not allow_value('abcd').for(:start_time) }
  it { should_not allow_value('abcd').for(:end_time) }

  it { should validate_numericality_of(:table_id) }

  it 'create right reservation (next time)' do
    @new_reservation.start_time += 4.hours
    @new_reservation.end_time += 6.hours
    expect(@new_reservation.save).to be(true)
  end
  
  it 'create right reservation (previous time)' do
    @new_reservation.start_time -= 6.hours
    @new_reservation.end_time -= 4.hours
    expect(@new_reservation.save).to be(true)
  end
  
  it 'create right reservation (anouther table)' do
    @new_reservation.table_id = 8
    expect(@new_reservation.save).to be(true)
  end
  
  it 'overlap reservation (inner)' do
    @new_reservation.start_time += 1.hours
    @new_reservation.end_time += 1.hours+30.minutes
    expect(@new_reservation.save).to be(false)
    expect(@new_reservation.errors).to have(1).error_on(:id)
  end
  
  it 'overlap reservation (outer)' do
    @new_reservation.start_time -= 1.hours
    @new_reservation.end_time += 3.hours
    expect(@new_reservation.save).to be(false)
    expect(@new_reservation.errors).to have(1).error_on(:id)
  end
  
  it 'overlap reservation (cross start_time)' do
    @new_reservation.start_time -= 1.hours
    @new_reservation.end_time += 1.hours
    expect(@new_reservation.save).to be(false)
    expect(@new_reservation.errors).to have(1).error_on(:id)
  end
  
  it 'overlap reservation (cross end_time)' do
    @new_reservation.start_time += 1.hours
    @new_reservation.end_time += 3.hours
    expect(@new_reservation.save).to be(false)
    expect(@new_reservation.errors).to have(1).error_on(:id)
  end

end
