require 'test_helper'

class ReservationTest < ActiveSupport::TestCase

  should validate_presence_of(:table_id).with_message('can\'t be blank')
  should validate_presence_of(:start_time).with_message('can\'t be blank')
  should validate_presence_of(:end_time).with_message('can\'t be blank')

  should validate_numericality_of(:table_id)

  should_not allow_value('abcd').for(:start_time)
  should_not allow_value('abcd').for(:end_time)

  should allow_value(DateTime.now).for(:start_time)
  should allow_value(DateTime.now).for(:start_time)

  context "Creating reservations" do
    setup do 
      @reservation = FactoryGirl.create(:reservation)
      @new_reservation = FactoryGirl.build(:reservation)
    end

    teardown do
      Reservation.destroy_all
    end

    should 'create right reservation (next time)' do
      @new_reservation.start_time += 4.hours
      @new_reservation.end_time += 6.hours
      assert_equal @new_reservation.save, true
    end
    
    should 'create right reservation (previous time)' do
      @new_reservation.start_time -= 6.hours
      @new_reservation.end_time -= 4.hours
      assert_equal @new_reservation.save, true
    end
    
    should 'create right reservation (anouther table)' do
      @new_reservation.table_id = 8
      assert_equal @new_reservation.save, true
    end
    
    should 'overlap reservation (inner)' do
      @new_reservation.start_time += 1.hours
      @new_reservation.end_time += 1.hours+30.minutes
      assert_equal @new_reservation.save, false
      assert_equal @new_reservation.errors.count, 1
    end
    
    should 'overlap reservation (outer)' do
      @new_reservation.start_time -= 1.hours
      @new_reservation.end_time += 3.hours
      assert_equal @new_reservation.save, false
      assert_equal @new_reservation.errors.count, 1
    end
    
    should 'overlap reservation (cross start_time)' do
      @new_reservation.start_time -= 1.hours
      @new_reservation.end_time += 1.hours
      assert_equal @new_reservation.save, false
      assert_equal @new_reservation.errors.count, 1
    end
    
    should 'overlap reservation (cross end_time)' do
      @new_reservation.start_time += 1.hours
      @new_reservation.end_time += 3.hours
      assert_equal @new_reservation.save, false
      assert_equal @new_reservation.errors.count, 1
    end
  end

end
